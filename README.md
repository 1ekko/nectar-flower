# Nectar Flower

![hummingbird](https://s3.amazonaws.com/wearenectar/static/hyperspace.jpg "hummingbird")

This is a very lightweight starter to create interfaces for web and mobile apps.  The big advantage this create is that there is only 1 codebase to support pretty much every phone/computer/os.  We use [Cordova](https://cordova.apache.org/) with a custom build system to offer a bridge and access to multiple native functionalities and commonly used tools.  We currently have <b>63</b> native plugin functionalities working together in both iOS and Android with a common javascript API.  This includes things like controlling the statusbar, qr code scanning, syncing events to your phones calendar, video calling, and much more.  Below you will find a list of links to all the plugins we are currently using.  Your apps will then be compatible with the nectar build system which could publish an app to the iOS and Android App stores.  Your app will also be able to work as an app through the already distributed [Nectar App](https://nectar.earth/download).  If you would like your own app to be distributed in the iOS and Android app stores, please [contact us](mailto:tom@nectar.earth).

Also worth noting, the development cycle should be fast as there is built in "hot reloading".  This means that as soon as you save in the text editor of your choice, the code will be synced to the browsers/apps you are currently viewing and update in real time.

# Getting Started

```
git clone https://repo.nectar.earth/ekko/nectar-flower
cd nectar-flower
npm install
serve-flower
```
Edit some content is one of the starter files and see it change in real time at [localhost:3333](http://localhost:3333).

# Mobile Apps for testing

The following apps work and connect with the development
[iOS Testflight App](https://testflight.apple.com/join/L2h1pbLe)
[Android Test App](https://nectar.earth/android_install/nectar_developer)

Relevant files
fileconf.json - this stores a configuration of what files you want to load into your app.  To ensure compatibility across multiple HTML5 based apps in the same container, we require your app to have a unique identifier.  This is specified in the "id" field in fileconf.json.  The "entry point" for your app will be at static/[app_id].js.  You must register your component like:
```
static/testapp.js
phi.loadApp('~app_id~',function(options){
	var self=this;
	this.init=function(){	
		phi.registerView('~app_id~','home',{},function(instance){
			instance.show();
		});
		_bootloader.hidePhotoSplash();
	}
	this.destroy=function(){//do any code to destroy this view
	}
});

static/views/home.js
phi.loadView('~app_id~','home',function(options){
	var self=this;
	this.show=function(){
		phi.render(domNode,{
			template:'~app_id~_home',
			append:false,
			binding:function(ele){
				self.ele=ele;
			}
		})
	}
}

fileconf.json
{
	"id":"testapp",
	"appEntry":{
	    "app_options":{}
	},
	"shared":{
		"ui":[],
		"modules":[]
	},
	"views":[
		"home"
	]
}
```
~app_id~ is a special variable that gets added into the code before loading it.  So ~app_id~ would be replaced by the "id":"" set in the fileconf.json.  Using this identifier allows systemic naming for things like javascript apps without conflicting and ensuring CSS rules do not conflict.
```
_bootloader.hidePhotoSplash();
```
Hides the splash screen that is part of the loading practice.  You should call this when you have a view ready to be seen.

# Object oriented nature

All apps and views are oriented as new instances of objects.  For simplicity, we almost always put var self=this at the top, just we we always know the context we are working in.  It is very easy to lose the context of "this" in javascript, so always knowing the "self" reference can help in navigating and linking different parts of code within the same context.

# Native Plugins

Please google these identifiers to find information about the plugins.  Fuller descriptions and links will be here soon.

com.danielsogl.cordova.clipboard
com.darktalker.cordova.screenshot
com.nikola-breznjak.voippush
cordova-android-support-gradle-release
cordova-call
cordova-plugin-actionsheet
cordova-plugin-add-swift-support
cordova-plugin-android-permissions
cordova-plugin-audiotoggle
cordova-plugin-background-mode
cordova-plugin-background-upload
cordova-plugin-badge
cordova-plugin-battery-status
cordova-plugin-calendar
cordova-plugin-call-number
cordova-plugin-camera
cordova-plugin-cocoapod-suppor
cordova-plugin-compat
cordova-plugin-contacts
cordova-plugin-customurlscheme
cordova-plugin-device
cordova-plugin-dialogs
cordova-plugin-disable-ios11-statusbar
cordova-plugin-facebook4
cordova-plugin-file
cordova-plugin-file-transfer
cordova-plugin-flashlight
cordova-plugin-fullscreen
cordova-plugin-geolocation
cordova-plugin-imagegallery
cordova-plugin-insomnia
cordova-plugin-ionic-keyboard
cordova-plugin-iosrtc
cordova-plugin-media
cordova-plugin-media-capture
cordova-plugin-nativestorage
cordova-plugin-network-information
cordova-plugin-networkinterface
cordova-plugin-qrscanner
cordova-plugin-safariviewcontroller
cordova-plugin-screen-orientation
cordova-plugin-screen-locker
cordova-plugin-speechrecognition
cordova-plugin-splashscreen
cordova-plugin-statusbar
cordova-plugin-timer
cordova-plugin-touch-id
cordova-plugin-vibration
cordova-plugin-wkwebview-engine
cordova-plugin-x-socialsharing
cordova-plugin-youtube-video-player
cordova-save-image-gallery
cordova-support-google-services
cordova.plugins.diagnostic
in.lucasdup.bringtofront
phonegap-plugin-mobile-accessibility
phonegap-plugin-multidex
phonegap-plugin-push
uk.co.workingedge.phonegap.plugin.launchnavigator

# Javascript Libraries that are included!

Here are a brief list of notable javascript code that will be available on the default "kitchen sink" app.  I dont have time to document them all right now, so please view your networks console when loading the app at [localhost:3333](http://localhost:3333).

jquery.js, ejs.js, async.js, md5.js, gsap.js, socket.io.js, d3.js, tinycolor.js, tinygradient.js, moment.js, howler.js, math.uuid.js