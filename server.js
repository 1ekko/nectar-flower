#!/usr/bin/env node
var watch = require('node-watch');
var express=require('express');
var fs = require('fs');
var app = express();
var http = require('http').createServer(app);
var Terser = require("terser");
var io = require('socket.io')(http);
var os = require('os');
var ifaces = os.networkInterfaces();
var local_IP=false;
var currentport=3333;
http.listen(3333, () => {
  console.log('listening on http://localhost:3333');
});// Object.keys(ifaces).forEach(function (ifname) {
//   var alias = 0;
var api={
  loadFiles:function(files,vars){
    var filesdata='';
    files.forEach(function(file) {
      try{
        filesdata+=fs.readFileSync(file,'utf8')
      }catch(e){
        console.log(e.message);
      }
    });
    if(vars&&filesdata){
      for(var key in vars){
        filesdata=filesdata.replace(new RegExp('~'+key+'~', 'g'),vars[key]);
      }
    }
    return filesdata;
  },
  getConfVars:function(conf){
    if(!conf.vars) conf.vars={};
    var vars=conf.vars;
    conf.vars.app_id=conf.id;
    return vars;
  },
  parseConf:function(conf){//build conf of js:[],css:[],templates:[]
    var fileconf={
      js:[],
      css:[],
      templates:[]
    }
    function getComponentFiles(path){
      var appjs=path+'.js';
      if (fs.existsSync(appjs)){
        fileconf.js.push(appjs);
      }
      var appcss=path+'.css';
      if (fs.existsSync(appcss)){
        fileconf.css.push(appcss);
      }
      var apptemplate=path+'.ejs';
      if (fs.existsSync(apptemplate)){
        fileconf.templates.push(apptemplate);
      }
      var apptemplate=path+'.templates';
      if (fs.existsSync(apptemplate)){
        fileconf.templates.push(apptemplate);
      }
    }
    //add in the app info first!
    getComponentFiles('static/'+conf.id);
    if(conf.views&&conf.views.length){
      for (var i = 0; i < conf.views.length; i++) {
        var view=conf.views[i];
        getComponentFiles('static/views/'+view+'/'+view);
      }
    }
    return fileconf;
  },
  parseConf2:function(conf){//build conf of js:'',css:'',templates:''
    var fileconf={
      js:'',
      css:'',
      templates:''
    }
    function getComponentFiles(appjs){
      if (fs.existsSync(appjs)){
        var content=fs.readFileSync(appjs,'utf8');
        var map={
          script:'js',
          templates:'templates',
          style:'css'
        }
        for (var key in map){
          var mapto=map[key];
          if(!fileconf[mapto]) fileconf[mapto]='';
          var start=content.search('<'+key+'>');
          if(start>=0){
            var end=content.search('</'+key+'>');
            if(end){
              var code=content.substring(start+('<'+key+'>').length,end);
              if(mapto=='js'){
                minified=Terser.minify(code,{});
                code=minified.code+';';
              }else{
                code=code.replace(/(\r\n|\n|\r|\t)/gm,"");
              }
              fileconf[mapto]+=code;
            }
          }
        }
      }else{
        console.log('File ['+appjs+'] does not exist');
      }
    }
    //add in the app info first!
    getComponentFiles('static/'+conf.id+'.app');
    if(conf.views&&conf.views.length){
      for (var i = 0; i < conf.views.length; i++) {
        var view=conf.views[i];
        getComponentFiles('static/views/'+view+'.view');
      }
    }
    var vars=api.getConfVars(conf);
    if(vars){
      for(var key in vars){
        for(var type in fileconf){
            fileconf[type]=fileconf[type].replace(new RegExp('~'+key+'~', 'g'),vars[key]);
        }
      }
    }
    if(fileconf.templates) fileconf.templates=addslashes(fileconf.templates)
    return fileconf;
  }
}
//   ifaces[ifname].forEach(function (iface) {
//     if ('IPv4' !== iface.family || iface.internal !== false) {
//       // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
//       return;
//     }

//     if (alias >= 1) {
//       // this single interface has multiple ipv4 addresses
//       console.log(ifname + ':' + alias, iface.address);
//       console.warn('We have not handled this case yet');
//     }else{
//       // this interface has only one ipv4 adress
//       if(iface.address){
//         console.log(`starting server at http://${iface.address}:${currentport}`)
//     		if(currentport==3333) http.listen(currentport,iface.address, () => function(){});
//         currentport++;
//     	}
//     }
//     ++alias;
//   });
// });
try{
  var conf=JSON.parse(api.loadFiles(['fileconf.json']));
}catch(e){
  console.log('Error processing fileconf.json');
  console.log(e);
  process.exit(0);
}
io.on('connection', (socket) => {
  console.log('connection!')
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', (data) => {
    console.log(data);
  });
});
//app.use('/static', express.static('static'));
app.get('/static/*',function(req,res){
  try{
      var conf=JSON.parse(api.loadFiles(['fileconf.json']));
      var path=req.originalUrl.substr(1);
      var pathp=path.split('?');//remove any QS
      var realpath=pathp[0];
      var extp=realpath.split('.');
      var ext=extp[extp.length-1];
      res.set("Access-Control-Allow-Origin", "*");
      res.set("Access-Control-Allow-Headers", "Content-Type,X-Requested-With");
      res.set("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
      switch(ext){
        case 'css':
          res.type("text/css");
        break;
        case 'js':
          res.type("application/javascript");
        break;
        case 'json':
          res.type("application/json");
        break;
      }
      res.send(api.loadFiles([realpath],api.getConfVars(conf)))
  }catch(e){
     res.send(JSON.stringify({'error':'error: '+e.message}));
  }
});
app.use('/dist', express.static('dist'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});
app.get('/ping', (req, res) => res.send(JSON.stringify({'success':true})))
app.get('/conf', (req, res) => {
    try{
      var conf=JSON.parse(api.loadFiles(['fileconf.json']));
      var appEntry=conf.appEntry;
      appEntry.app=conf.id;
      //var fileconf=api.parseConf(conf);
      //   res.send(JSON.stringify({'success':true,'conf':{
      //       "key":conf.id,
      //       "urls":{
      //         "js":fileconf.js,
      //         "css":fileconf.css,
      //       },
      //       "templates":addslashes(api.loadFiles(fileconf.templates,api.getConfVars(conf)).replace(/(\r\n|\n|\r|\t)/gm,"")),
      //       "entry":conf.entry,
      //       "appEntry":appEntry
      //     }
      // }));
      var fileconf=api.parseConf2(conf);
      res.send(JSON.stringify({'success':true,'conf':{
            "key":conf.id,
            "code":fileconf,
            "entry":conf.entry,
            "appEntry":appEntry
          }
      }));
    }catch(e){
      res.send(JSON.stringify({'error':'error: '+e.message}));
    }
})
app.get('*', function(req, res){
  res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Headers", "Content-Type,X-Requested-With");
    res.set("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.type("text/html");
    res.send(api.loadFiles(['app.html'],api.getConfVars(conf)))
});
var files2={
  'static':'static',
  'server':'server.js'
}
for (var prop in files2) {
  var file=files2[prop];
  watch(file, { recursive: true },function(event, filename){
    console.log(event+ ' '+filename);
    var extp=filename.split('.');
    var ext=extp[extp.length-1];
    io.emit('dev_channel',{
        type:'fileupdate',
        data:{
          app:conf.id,
          file:filename,
          ext:ext
        }
      });
  });
}
function addslashes(string) {
    return string.replace(/\\/g, '\\\\').
        replace(/\u0008/g, '\\b').
        replace(/\t/g, '\\t').
        replace(/\n/g, '\\n').
        replace(/\f/g, '\\f').
        replace(/\r/g, '\\r').
        replace(/'/g, '\\\'').
        replace(/"/g, '\\"');
}